package com.example.hellomongodb.Config;

import java.util.HashMap;

import com.example.hellomongodb.domain.Employee;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.data.MongoItemReader;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
@EnableBatchProcessing
public class MongoDBReader {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private MyItemWriterBuilder<Employee> writer;

    @Bean
    public CustomPropertiesConfig getConfig() {
        return new CustomPropertiesConfig();
    }

    private final String csvPath = getConfig().getMyExportCsvFile() + "/employees.csv";

    @Bean
    public Job readEmployee() {
        return jobBuilderFactory.get("readEmployee").flow(step1()).end().build();
    }

    @Bean
    public Step step1() {
        return stepBuilderFactory.get("step1").<Employee, Employee>chunk(10).reader(reader())
                .writer(writer.writer(csvPath)).build();
    }

    @Bean
    /*TODO: Try to do the same with the reader*/
    public MongoItemReader<Employee> reader() {
        MongoItemReader<Employee> reader = new MongoItemReader<>();
        reader.setTemplate(mongoTemplate);
        reader.setSort(new HashMap<String, Sort.Direction>() {{
            put("_id", Direction.ASC);
        }});
        reader.setTargetType(Employee.class);
        reader.setQuery("{}");
        System.out.println(" Hello " + reader);
        return reader;
    }
}
