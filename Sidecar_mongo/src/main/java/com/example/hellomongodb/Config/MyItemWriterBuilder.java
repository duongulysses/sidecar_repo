package com.example.hellomongodb.Config;

import org.springframework.batch.item.file.FlatFileItemWriter;

public abstract class MyItemWriterBuilder<IN> {
    public abstract FlatFileItemWriter<IN> writer(String csvName);
}
