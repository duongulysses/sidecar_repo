package com.example.hellomongodb.Config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "spring.my.custom")
@Configuration
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomPropertiesConfig {
    private String myExportCsvFile;
}
