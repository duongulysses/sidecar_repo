package com.example.hellomongodb.Config;

import com.example.hellomongodb.domain.Employee;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.BeanWrapperFieldExtractor;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Component;

@Component
public class EmployeeReaderBuilder extends MyItemWriterBuilder<Employee> {
    @Override
    public FlatFileItemWriter<Employee> writer(String csvPath) {
        FlatFileItemWriter<Employee> writer = new FlatFileItemWriter<>();
        writer.setResource(new FileSystemResource(csvPath));
        writer.setAppendAllowed(true);
        writer.setLineAggregator(new DelimitedLineAggregator<Employee>() {
            {
                setDelimiter(",");
                setFieldExtractor(new BeanWrapperFieldExtractor<Employee>() {
                    {
                        setNames(new String[]{"id", "firstName", "lastName", "department", "salary"});
                    }
                });
            }
        });
        return writer;
    }
}
