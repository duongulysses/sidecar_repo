package com.example.hellomongodb.domain;

import java.math.BigDecimal;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

@Data
@Document(collection = "employee")
public class Employee {
    @Id
    private int id;
    private String firstName;
    private String lastName;
    private String depertment;
    private BigDecimal salary;
}
